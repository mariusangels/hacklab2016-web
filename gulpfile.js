const elixir = require('laravel-elixir');

elixir(function (mix) {
    mix.sass('app.scss')
       .webpack('dashboard/app.js', './public/js/dashboard.js')
       .browserSync({
            proxy: 'hacklab.dev'
       });
});
