module.exports = {
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    "presets": [
                        "react",
                        "es2015",
                        "stage-1"
                    ],
                    "plugins": ["transform-decorators-legacy"]
                }
            }
        ]
    }
}
