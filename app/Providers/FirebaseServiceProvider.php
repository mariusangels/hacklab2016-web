<?php

namespace App\Providers;

use Kreait\Firebase\Configuration;
use Kreait\Firebase\Firebase;
use Illuminate\Support\ServiceProvider;

class FirebaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('firebase', function ($app) {
            $config = new Configuration();
            $config->setAuthConfigFile(base_path('firebase-service-account.json'));
            return new Firebase('https://hacklab-8aa28.firebaseio.com/', $config);
        });
    }
}
