<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = ['name', 'description', 'cover_url',
    'amount_1', 'amount_2', 'amount_3', 'thank_you_msg',
    'decline_msg', 'latest_update', 'user_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }
}
