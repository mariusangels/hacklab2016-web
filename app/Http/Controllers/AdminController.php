<?php

namespace App\Http\Controllers;

use Gate;
use App;
use Auth;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (Gate::denies('is-admin', $user)) {
            return redirect('/');
        }
        return view('admin.dashboard');
    }
}
