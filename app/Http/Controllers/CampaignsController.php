<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Campaign;
use App\Http\Requests;

class CampaignsController extends Controller
{
    public function get() {
        $campaigns = Campaign::all();
        $campaigns->load('user');
        return $campaigns;
    }

    public function create(Request $request)
    {
        $data = $request->all();
        if ($request->hasFile('campaign_cover')) {
            $image = $request->file('campaign_cover');
            $dest_path = base_path('public/campaign_images/');
            $filename = uniqid() . '.' . $image->guessClientExtension();
            $image->move($dest_path, $filename);
            $data['cover_url'] = asset('campaign_images/' . $filename);
        }
        $data['user_id'] = Auth::user()->id;

        Campaign::create($data);
        return back()->with('success', 'Your campaign has been successfully created!');
    }
}
