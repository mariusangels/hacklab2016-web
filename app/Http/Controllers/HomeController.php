<?php

namespace App\Http\Controllers;

use Auth;
use View;
use App\Campaign;
use Illuminate\Http\Request;

use App\Http\Requests;

class HomeController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->middleware('auth');
        
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            View::share('user', $this->user);

            return $next($request);
        });
    }

    public function index()
    {
        $campaigns = $this->user->campaigns()->get();
        return view('org.home')->with(['campaigns' => $campaigns]);
    }
}
