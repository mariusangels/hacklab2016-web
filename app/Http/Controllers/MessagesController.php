<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class MessagesController extends Controller
{
    public function get() {
        return [
          [
            "text" => "Welcome to Ambag"
          ],
          [
            "text" => "Thank you for using our app. It’s a conversation and story driven platform about supporting non-profit organisations. Organisations can send you messages about their causes and you can respond by tapping the buttons that appear",
            "replyChoice" => [
              [
                "text" => "Like this?",
                "action" => "ask"
              ]
            ]
          ],
          [
            "text" => "Yes, just like that. Open the app whenever you want, and you’ll get updates from the charities/organisations you supported. You can also earn karma points. It increases every time you make a donation or share a cause. Are you ready to get started?",
            "replyChoice" => [
              [
                "text" => "Let's do this!",
                "action" => "ask"
              ]
            ]
          ],
          [
            "text" => "First, choose a non-profit organisation. You can do this by going to the org page, located to the left. Swipe over there anytime.",
            "replyChoice" => [
              [
                "text" => "Ok",
                "action" => "ask"
              ]
            ]
          ],
          [
            "text" => "Great! Now you selected your first charity organisation. They will send you a message shortly. Just remember, the search charities page is located to the left and your selected charities is located to the right. You can swipe over there anytime. Your supported org is now sending you a message.",
            "replyChoice" => [
              [
                "text" => "Thanks!",
                "action" => "ask"
              ]
            ]
          ]
        ];
    }
}
