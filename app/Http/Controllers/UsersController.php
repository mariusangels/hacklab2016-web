<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;

class UsersController extends Controller
{
    function get()
    {
        return User::all();
    }

    function update(Request $request, User $user)
    {
        $data = $request->all();
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $dest_path = base_path('public/avatars/');
            $filename = uniqid() . '.' . $avatar->guessClientExtension();
            $avatar->move($dest_path, $filename);
            $data['avatar_url'] = asset('avatars/' . $filename);
        }

        unset($data['avatar']);
        unset($data['_token']);
        $result = $user->update($data);
        return back();
    }
}

// $firebase = App::make('firebase');
// return $firebase->get('/users');