<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;

class OrganizationsController extends Controller
{
    public function get() {
        $users = User::all();
        $output = [];
        
        foreach ($users as $user) {
            array_push($output, [
                "name" => $user->organization,
                "desc" => $user->description,
                "fundraiserName" => $user->name,
                "fundraiserPhotoURL" => $user->avatar_url,
            ]);
        }

        return $output;
    }
}
