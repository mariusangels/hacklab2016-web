@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" id="app"></div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ url('js/dashboard.js') }}"></script>
@endsection
