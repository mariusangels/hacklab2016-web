<!DOCTYPE html>

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Ambag - Your social platform for sharing</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|Source+Serif+Pro" rel="stylesheet">
    <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.6.3/font-awesome.min.css" integrity="sha384-Wrgq82RsEean5tP3NK3zWAemiNEXofJsTwTyHmNb/iL3dP/sZJ4+7sOld1uqYJtE" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('landing/css/default.css') }}">
    <link rel="stylesheet" href="{{ asset('landing/css/ambag.css') }}">
</head>
<body>
    <!-- dashboard nav -->
    <nav class="navbar navbar-fixed-top" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">
        <img src="{{ url('img/logo-small.png') }}" alt="">
      </a>
  </div>
</li>
</ul>

</nav>
<!-- start banner  -->
<section class="banner text-center grandient-overlay" id="sec1">
  <div class="container">
    <div class="row">
      <h1>Small Gestures. Big Impact</h1>
      <p>Supporting charities and educational causes through inspirational stories</p>
    <!-- button trigger modal -->
    <p><a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mymodal" role="button">Start Fundraising</a> </p>
    <!-- end button modal -->

</div>
</div>
</section>
<!-- end banner -->

<!-- start about -->

<section class="about text-center" id="sec2">
  <div class="container">
    <div class="row push-bottom">
      <div class="col-md-8 col-md-offset-2">
          <h2>About</h2>
          <p>Ambag is a social platform for giving and supporting educational causes.
          Our mission is to ensure no great cause goes unfunded.
          Are you a non-profit educational organization, institution  or community that needs aid?
          We help you get donations by helping you telling your stories effectively.</p>
      </div>
    </div>

    <hr>

    <div class="row push-bottom">
      <div class="col-md-12">
        <h2>How does it work?</h2>
      </div>
    </div>

    <div class="col-md-3 single-about col-xs-6 col-sm-4">
        <div class="about-inside">
          <i class="fa fa-lightbulb-o"></i>
          <h3>Register and tell your story</h3>
          <p>Just register on the website and start telling us about your story. What's your organization or cause? who are you helping? </p>
      </div>
  </div>

<section class="push-bottom">
  <div class="container">
    <div class="row">
      <div class="col-md-3 single-about col-xs-6 col-sm-4">
        <div class="about-inside">
            <i class="fa fa-pencil"></i>
            <h3>Get feedback on your campaign</h3>
            <p>Set target funding and get initial support from friends and families. Tell them about your cause by either giving feedback on your campaign</p>
        </div>
      </div>
      
      <div class="col-md-3 single-about col-xs-6 col-sm-4">
        <div class="about-inside">
          <i class="fa fa-cog"></i>
          <h3>Share to the world!</h3>
          <p>Spread the word about your cause. Send to to friends and tell them to send to their friends. Every supporter gets good karma points.</p>
        </div>
      </div>
      
      <div class="col-md-3 single-about col-xs-6 col-sm-4">
        <div class="about-inside">
          <i class="fa fa-laptop"></i>
          <h3>Track your campaign</h3>
          <p>See the progress of your campaign. Learn about what people thing from their donation pledges.</p>
        </div>
      </div>
    </div>
  </div>
</section>

</section>

<!-- end about -->
<section class="banner text-center" id="sec2">
  <div class="container">
    <div class="row">
      <h1>What are you waiting for?</h1>
      <p>
        Get funded now!
    </p>
    <!-- button trigger modal -->
    <p><a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#mymodal" role="button">Start Fundraising</a> </p>
    <!-- end btton modal -->
</div>
</div>
</section>

<!-- footer start -->
<hr />

<footer class="footer text-center">
  <div class="footer-social">
    <div class="container text-center">
        <ul class="list-inline">
            <li class="social-github">
                <a href="#"><i class="fa fa-github"></i></a>
            </li>
            <li class="#">
                <a href="#"><i class="fa fa-twitter"></i></a>
            </li>
            <li class="social-facebook">
                <a href="#"><i class="fa fa-facebook"></i></a>
            </li>
            <li class="social-google-plus">
                <a href="#"><i class="fa fa-google-plus"></i></a>
            </li>
        </ul>
    </div>
</div>
<p>Copyright: © Ambag | All Rights Reserved</p>
</footer>
<br><br><br>
<!-- end footer -->

<!-- modal -->
<div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="mymodallabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
      <div class="modal-body">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Password</label>

                <div class="col-md-6">
                    <input id="password" type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Login
                    </button>

                    <a class="btn btn-link" href="{{ url('/register') }}">
                        Register
                    </a>
                </div>
            </div>
        </form>
      </div>
  </div>
</div>
</div>

<script src="{{ asset('landing/js/vendors.min.js') }}"></script>

</body>
</html>
