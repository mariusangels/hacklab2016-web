@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">

    <div id="page-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <h1 class="page-header">
              Dashboard <small>Statistics Overview</small>
            </h1>

          </div>
        </div>

        @if (session('success'))
        <div class="row">
          <div class="col-lg-12">
            <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <p>{{ session('success') }}</p>
            </div>
          </div>
        </div>
        @endif

        <div class="row">
          <div class="col-lg-12">
            <div class="alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <i class="fa fa-info-circle"></i> Looks like this is your first time.  Start your <a href="#">pitch</a> to campaign donors now.
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-comments fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">$0.00</div>
                    <div>Total Donations</div>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="panel panel-green">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-tasks fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">0</div>
                    <div>Donors</div>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="panel panel-yellow">
              <div class="panel-heading">
                <div class="row">
                  <div class="col-xs-3">
                    <i class="fa fa-shopping-cart fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                    <div class="huge">0</div>
                    <div>Ongoing campaign</div>
                  </div>
                </div>
              </div>
              <a href="#">
                <div class="panel-footer">
                  <span class="pull-left">View Details</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
                </div>
              </a>
            </div>
          </div>

        </div>
        <!-- end inital badges -->
        <!--start story collection-->
        <div class="row">
          <div class="col-lg-12">
            <h3>Your Story</h3>

            <hr />

            <form method="post" class="form" enctype="multipart/form-data" action="{{ url('users/' . $user->id) }}">

              {{ csrf_field() }}

              <div class="form-group">
                <label for="storyTellerName">Hello, my name is</label>
                <input type="text" name="name" class="form-control"
                  id="storyTellerName" placeholder="John" size="35" value="{{ $user->name }}">
              </div>

              <div class="form-group">
                <label for="storyTellerJob">I am currently the founder of</label>
                <input type="text" name="organization" class="form-control"
                  id="storyTellerJob" placeholder="your organization" size="35" value="{{ $user->organization }}">
              </div>

              <div class="form-group">
                <label for="storyTellerDetails">We have been..</label>
                <textarea class="form-control" name="description" id="storyTellerDetails"
                  placeholder="helping out of school youth. We started our community last 2010 and have helped around 30 young uneducated girls.  "
                  rows="3">{{ $user->description }}</textarea>
              </div>

              <div class="form-group">
                <label for="storyTellerPicture">Avatar</label>
                <input type="file" class="form-control" id="storyTellerPicture" name="avatar">
                @if ($user->avatar_url)
                  <div style="margin-top: 10px">
                    <img src="{{ $user->avatar_url}}" alt="User Avatar">
                  </div>
                @endif
              </div>
              <br />
              <br />
              <button type="submit" class="btn btn-primary">Update</button>
            </form>

          </div>
        </div>

        <!-- end story -->
        <hr />

        @if ($campaigns)
        <div class="row">
          <div class="col-lg-12">
            <h3>My Campaigns</h3>

            <div class="row">
              @foreach ($campaigns as $campaign)
              
                <div class="col-md-4">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <h4>{{ $campaign->name }}</h4>
                      <p>{{ $campaign->description }}</p>
                    </div>
                  </div>
                </div>
              
              @endforeach
            </div>
          </div>
        </div>
        <hr />
        @endif


        <!-- start campaigns -->
        <div class="row" style="padding-bottom: 100px">
          <div class="col-lg-12">
            <h3>Create New Campaign</h3>
            <div id="accordion" role="tablist" aria-multiselectable="true">
              <div class="">
                <div class="row">
                  <div class="col-md-12">
                    <div id="collapseOne" class=" collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <form method="post" class="form" enctype="multipart/form-data" action="{{ url('campaigns') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label for="campaignName">One of our campaings I called..</label>
                          <input type="text" class="form-control" id="campaignName" placeholder="Bookreaders Campaign"
                            name="name" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                          <label for="campaignDetails">Let me tell you the details..</label>
                          <textarea class="form-control" id="campaignDetails" rows="3" placeholder="We're basically buying books for the kids this coming semester."
                            name="description" value="{{ old('description') }}"></textarea>
                        </div>
                        <div class="input-group">
                          <label for="campaignCover">Here's a picture</label>
                          <input type="file" class="form-control" id="campaignCover" name="campaign_cover">
                        </div>
                        <br />
                        <p>How much would you like to donate? [Select how much donors can donate:]</p>
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="$5.00"
                            name="amount_1" value="{{ old('amount_1') }}">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="$15.00"
                            name="amount_2" value="{{ old('amount_2') }}">
                        </div>
                        <div class="form-group">
                          <input type="text" class="form-control" placeholder="$25.00"
                            name="amount_3" value="{{ old('amount_3') }}">
                        </div>
                        <p>[If donors gives cash]</p>
                        <div class="form-group">
                          <input type="text" class="form-control" id="thanks" placeholder="Thank you very much for your donation!"
                            name="thank_you_msg" value="{{ old('thank_you_msg') }}">
                        </div>

                        <p>[If donors declines]</p>
                        <div class="form-group">
                          <input type="text" class="form-control" id="thanks" placeholder="There are other ways to help us out! Why don't you spead the word..."
                            name="decline_msg" value="{{ old('decline_msg') }}">
                        </div>

                        <div class="form-group">
                          <label for="campaignDetails">Update</label>
                          <textarea class="form-control" id="campaignDetails" rows="3" placeholder="Latest update is..."
                            name="latest_update">
                            {{ old('latest_update') }}
                          </textarea>
                          </div
                          <br />
                          <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
    @endsection
