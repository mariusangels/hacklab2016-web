import React, { Component } from 'react'
import { observer } from 'mobx-react'

@observer class App extends Component {
    constructor(props) {
        super()
        this.store = props.store
    }

    componentWillMount() {
        this.store.dashboard.getUsers()
    }

    render() {
        const isLoading = this.store.dashboard.isLoading
        const users = this.store.dashboard.users

        return (
            <div>
                <h1>Users</h1>
                <div className="controls">
                    <form className="form-inline">
                        <div className="form-group">
                            <label htmlFor="name">Name</label>
                            <input type="text" className="form-control" id="name" placeholder="Juan dela Cruz"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="email" className="form-control" id="email" placeholder="you@domain.com"/>
                        </div>
                        <button type="submit" className="btn btn-primary">+ Create New User</button>
                    </form>
                </div>
            </div>
        )
    }
}

export { App }
