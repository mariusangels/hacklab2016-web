import { reaction, observable, observe, computer, autoron } from 'mobx'
import autobind from 'autobind-decorator'
import axios from 'axios'

@autobind
class DashboardStore {
    @observable isLoading = true
    @observable users = []

    getUsers() {
        axios.get('/api/users')
            .then(response => {
                this.users = response.data
                this.isLoading = false;
                console.log(this.users.peek())
                console.log(this.isLoading)
            })
            .catch(error => console.error(error))
    }
}

let store = {
    dashboard: new DashboardStore
}

export default store
